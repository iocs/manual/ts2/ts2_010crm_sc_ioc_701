#
# Module: essioc
#
require essioc

#
# Module: tdklambdaz
#
require tdklambdaz


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${tdklambdaz_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-010CRM:Cryo-PSU-010
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_master.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-010, IPADDR = ts2-cryo-tdklambda1.tn.esss.lu.se, RS485_ADDR = 1")

#
# Device: TS2-010CRM:Cryo-PSU-011
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-011, MASTER = TS2-010CRM:Cryo-PSU-010, RS485_ADDR = 2")

#
# Device: TS2-010CRM:Cryo-PSU-012
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-012, MASTER = TS2-010CRM:Cryo-PSU-010, RS485_ADDR = 3")

#
# Device: TS2-010CRM:Cryo-PSU-013
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-013, MASTER = TS2-010CRM:Cryo-PSU-010, RS485_ADDR = 4")

#
# Device: TS2-010CRM:Cryo-PSU-020
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-020, MASTER = TS2-010CRM:Cryo-PSU-010, RS485_ADDR = 5")

#
# Device: TS2-010CRM:Cryo-PSU-021
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-021, MASTER = TS2-010CRM:Cryo-PSU-010, RS485_ADDR = 6")

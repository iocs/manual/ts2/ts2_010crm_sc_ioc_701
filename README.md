# IOC to control TDK Lambda Z series power supplies

## Used modules

*   [tdklambdaz](https://gitlab.esss.lu.se/e3/wrappers/ps/e3-tdklambdaz.git)


## Controlled devices

*   TS2-010CRM:Cryo-PSU-010
    *   TS2-010CRM:Cryo-PSU-011
    *   TS2-010CRM:Cryo-PSU-012
    *   TS2-010CRM:Cryo-PSU-013
    *   TS2-010CRM:Cryo-PSU-020
    *   TS2-010CRM:Cryo-PSU-021
